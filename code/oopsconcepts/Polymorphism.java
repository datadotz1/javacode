package oopsconcepts;

public class Polymorphism {

		void Android()
		{
			System.out.println("It is a old version");
		}
		public static void main(String[] args) {
			Polymorphism o = new Polymorphism1();
			o.Android();
	}
}
class Polymorphism1 extends Polymorphism{
	void Android() //override the method
	{
		System.out.println("It is a newer version");
	}
		

}
