package oopsconcepts;

abstract class Abstraction {
	abstract void Hostel(); //Abstract  method
	
	void exam(){ //Non Abstract  method
		System.out.println("as per uni");
	}
	
	public static void main(String[] args) {
		Principal a = new Principal();
		a.Hostel();
		a.Transport();
		a.exam();
		
	}
}

class Dean extends Abstraction{
	
	void Hostel(){
		System.out.println("Hostel is not mandatory");
	}
	
	void Transport(){
		System.out.println("Transport is not mandatory");
	}
	}

class Principal extends Dean{
	
void Hostel(){
		System.out.println("Hostel is mandatory");
	}

void Transport(){
		System.out.println("Transport is mandatory");
	}
}
