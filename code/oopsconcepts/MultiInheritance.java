package oopsconcepts;

public class MultiInheritance {
	void GrandFather()
	{
		System.out.println("It is a GrandFather Class");
	}
	public static void main(String[] args) {
		MultiInheritance2 a = new MultiInheritance2();
		a.Son();
		a.Father();
		a.GrandFather();
	}
}
class MultiInheritance1 extends MultiInheritance{
	void Father(){
		System.out.println("It is a Father Class");
	}
}
class MultiInheritance2 extends MultiInheritance1{
	void Son(){
		System.out.println("It is a son Class");
	}
}