package exceptionhandling;

import java.io.IOException;

public class ThrowsKey {
	void ab() throws IOException
	{
		throw new IOException("Error");
	}
	void bc() throws IOException{
		ab();
	}
	
void cd(){
		try{
			bc();
		}
catch(Exception e)
	{
		System.out.println("Exception Handled");
	}
}
	public static void main(String[] args) {
		ThrowsKey a = new ThrowsKey();
		a.cd();
		
System.out.println("Normal flow");
	}
}
