package exceptionhandling;

public class CatchMulti {

	public static void main(String[] args) {
		try{
			int a[] = new int [5];
			a[5] = 30;
			System.out.println(a[7]);
			
			//manually throw the exception
			throw new ArrayIndexOutOfBoundsException("unbound value");
			
			
		}
		catch(ArithmeticException e){
			
			System.out.println("task1 is completed");
			
			}  
		   catch(ArrayIndexOutOfBoundsException e){
			   System.out.println("task 2 completed");
			   }  
		   catch(Exception e){
			   System.out.println("common task completed"+e);
			   }  
	}

}
