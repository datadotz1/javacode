package collection;

import java.util.HashMap;
import java.util.Map;

public class Hashmap {

	public static void main(String[] args) {
		Map<Integer,String> al = new HashMap<Integer,String>();
		al.put(100, "Ravi");
		al.put(24,  "Raju");
		al.put(1200,"Ravi");
		al.put(169, "Ajay");
		al.put(75,  "Azar");
		al.put(608, "Fayaz");
		al.put(null,"Siva");
		al.put(null,"Ram");
		al.put(1110, null);
		al.put(1111, null);
		System.out.println(al);

	for(Map.Entry<Integer, String>u : al.entrySet()){
		System.out.println(u.getKey()+"  "+u.getValue());
	}

	}

}
