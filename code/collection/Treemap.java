package collection;

import java.util.Map;
import java.util.TreeMap;

public class Treemap {

	public static void main(String[] args) {
		TreeMap<Integer,String> al = new TreeMap<Integer,String>();
		al.put(100, "Sathish");
		al.put(122, "Raja");
		al.put(151, "Akbar");
		al.put(115, "Vikram");
		//al.put(null, "Azar");
		//al.put(null, "Vignesh");
		al.put(199, null);
		al.put(116, null);
		for(Map.Entry<Integer,String> u : al.entrySet()){
			System.out.println(+u.getKey()+" "+u.getValue());
		}

	}

}
